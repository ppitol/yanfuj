package yanfuj.base.collections;

import static yanfuj.base.Option.option;
import static yanfuj.base.Preconditions.notNull;

import java.util.Collections;
import java.util.Iterator;

import yanfuj.functions.F;

class MappingIterable<S, T> implements Iterable<T> {

	private final F<? super S, ? extends T> mapper;
	private final Iterable<S> it;

	MappingIterable(F<? super S, ? extends T> mapper, Iterable<S> it) {
		this.mapper = notNull(mapper);
		this.it = option(it).getOrElse(Collections.<S>emptyList());
	}

	@Override
	public Iterator<T> iterator() {
		return new UnmodifiableIterator<T>() {

			private final Iterator<S> iter = it.iterator();

			@Override
			public boolean hasNext() {
				return iter.hasNext();
			}

			@Override
			public T next() {
				return mapper.apply(iter.next());
			}
		};
	}
}
