package yanfuj.base.collections;

import static yanfuj.base.Option.option;
import static yanfuj.base.Preconditions.notNull;
import static yanfuj.base.Some.some;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import yanfuj.base.None;
import yanfuj.base.Option;
import yanfuj.functions.F;
import yanfuj.functions.F2;
import yanfuj.functions.P1;

/**
 * Functions over {@link Iterable}s
 *
 * @author pitol
 */
public class Iterables {

    /**
     * Apply the procedure to each element of the {@link Iterable}.
     */
    public static <T> void forEach(P1<? super T> procedure, Iterable<T> it) {
        P1<? super T> p = notNull(procedure);
        for (T element : notNull(it)) {
            p.exec(element);
        }
    }

    /**
     * @return the head element, {@link None} for an empty or null
     *         {@link Iterable}.
     */
    public static <T> Option<T> first(Iterable<T> it) {
        Iterator<T> iter = option(it).getOrElse(Collections.<T> emptyList()).iterator();
        return iter.hasNext() ? some(iter.next()) : None.<T> none();
    }

    /**
     * @return lazy {@link Iterable} over the remaining elements after the first,
     *         empty for null or empty Iterable (or single element, of course).
     */
    public static <T> Iterable<T> rest(Iterable<T> it) {
        Iterator<T> iter = option(it).getOrElse(Collections.<T> emptyList()).iterator();
        return iter.hasNext() ? new RestIterable<T>(it) : Collections.<T> emptyList();
    }

    /**
     * Lazilly apply a mapping over an {@link Iterable}.
     */
    public static <S, T> Iterable<T> map(F<? super S, ? extends T> mapper, Iterable<S> it) {
        return new MappingIterable<S, T>(mapper, it);
    }

    /**
     * Lazilly filter the {@link Iterable}.
     */
    public static <T> Iterable<T> filter(F<? super T, Boolean> filter, Iterable<T> it) {
        return new FilteringIterable<T>(filter, it, true);
    }

    /**
     * Lazilly exclude (filter out the positive matches) on the {@link Iterable}
     * .
     */
    public static <T> Iterable<T> exclude(F<? super T, Boolean> filter, Iterable<T> it) {
        return new FilteringIterable<T>(filter, it, false);
    }

    /**
     * Reduce the {@link Iterable}
     */
    public static <R, T> R reduce(F2<? super R, ? super T, ? extends R> reducer, R seed, Iterable<T> it) {
        R acumulator = seed;
        for (T element : option(it).getOrElse(Collections.<T>emptyList())) {
            acumulator = reducer.apply(acumulator, element);
        }
        return acumulator;
    }

    /**
     * Fold left the {@link Iterable}
     * @return {@link None} for empty or null {@link Iterable}
     */
    public static <T> Option<T> fold(F2<? super T, ? super T, ? extends T> folder, Iterable<T> it) {
        return option(reduce(folder, first(it).getOrElse(null), rest(it)));
    }

    /**
     * Group the elements according to a key mapping function.
     */
    public static <K, T> Map<K, List<T>> group(final F<? super T, ? extends K> keymapper, final Iterable<T> it) {
        F2<Map<K, List<T>>, T, Map<K, List<T>>> mapper = new F2<Map<K, List<T>>, T, Map<K, List<T>>>() {
            @Override
            public Map<K, List<T>> apply(Map<K, List<T>> map, T element) {
                K key = keymapper.apply(element);
                List<T> list = option(map.get(key)).getOrElse(new LinkedList<T>());
                list.add(element);
                map.put(key, list);
                return map;
            }
        };

        return reduce(mapper, new HashMap<K, List<T>>(), it);
    }
}
