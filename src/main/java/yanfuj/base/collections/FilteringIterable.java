package yanfuj.base.collections;

import static yanfuj.base.Option.option;
import static yanfuj.base.Preconditions.notNull;
import static yanfuj.functions.Predicates.not;

import java.util.Collections;
import java.util.Iterator;
import java.util.NoSuchElementException;

import yanfuj.functions.F;

class FilteringIterable<T> implements Iterable<T> {

	private final F<? super T, Boolean> filter;
	private final Iterable<T> it;
	
	FilteringIterable(F<? super T, Boolean> filter, Iterable<T> it, boolean include) {
		F<? super T, Boolean> f = notNull(filter);
		if (include) {
			this.filter = f;
		} else {
			this.filter = not(f);
		}
		this.it = option(it).getOrElse(Collections.<T>emptyList());
	}

	@Override
	public Iterator<T> iterator() {
		return new UnmodifiableIterator<T>() {

			private final Iterator<T> iter = it.iterator();
			private T next;
			private boolean foundNext = false;
			
			@Override
			public boolean hasNext() {
				while (!foundNext && iter.hasNext()) {
					T val = iter.next();
					if (filter.apply(val)) {
						next = val;
						foundNext = true;
					}
				}
				return foundNext;
			}

			@Override
			public T next() {
				if (!foundNext) {
					throw new NoSuchElementException();
				}
                foundNext = false;
				return next;
			}
		};
	}
}
