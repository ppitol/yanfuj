package yanfuj.base.collections;

import java.util.Iterator;

/**
 * Base class for unmodifiable {@link Iterator}s
 * 
 * @author pitol
 * 
 * @param <E>
 *            the element type
 */
public abstract class UnmodifiableIterator<E> implements Iterator<E> {

	/**
	 * @throws UnsupportedOperationException
	 *             always
	 */
	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
}
