package yanfuj.base.collections;

import java.util.Iterator;

class RestIterable<T> implements Iterable<T> {

	private final Iterable<T> it;
	
	RestIterable(Iterable<T> it) {
		this.it = it;
	}

	@Override
	public Iterator<T> iterator() {
		return new UnmodifiableIterator<T>() {
			
			private final Iterator<T> iter;
			{
				iter = it.iterator();
				iter.next();
			}

			@Override
			public boolean hasNext() {
				return iter.hasNext();
			}

			@Override
			public T next() {
				return iter.next();
			}
		};
	}
}
