package yanfuj.base;

import java.lang.reflect.Array;

/**
 * Collection of static methods to assist in checking arguments preconditions
 * 
 * @author pitol
 */
public class Preconditions {

    /**
     * Enforce a not-null precondition.
     * 
     * @param o
     *            Can't be null
     * @return The argument itself
     * @throws IllegalArgumentException
     *             if o is null.
     */
    public static <T> T notNull(T o) {
        if (o == null) {
            throw new IllegalArgumentException("null");
        }
        return o;
    }

    private static <T> T validateArray(T array) {
        notNull(array);
        if (Array.getLength(array) == 0) {
            throw new IllegalArgumentException("empty");
        }
        return array;
    }

    /**
     * Enforce a not-null and not-empty precondition on an array.
     * 
     * @param array
     *            Can't be null or empty
     * @return The argument itself
     * @throws IllegalArgumentException
     *             if array is null or empty.
     */
    public static byte[] notEmpty(byte[] array) {
        return validateArray(array);
    }

    /**
     * Enforce a not-null and not-empty precondition on an array.
     * 
     * @param array
     *            Can't be null or empty
     * @return The argument itself
     * @throws IllegalArgumentException
     *             if array is null or empty.
     */
    public static short[] notEmpty(short[] array) {
        return validateArray(array);
    }

    /**
     * Enforce a not-null and not-empty precondition on an array.
     * 
     * @param array
     *            Can't be null or empty
     * @return The argument itself
     * @throws IllegalArgumentException
     *             if array is null or empty.
     */
    public static char[] notEmpty(char[] array) {
        return validateArray(array);
    }

    /**
     * Enforce a not-null and not-empty precondition on an array.
     * 
     * @param array
     *            Can't be null or empty
     * @return The argument itself
     * @throws IllegalArgumentException
     *             if array is null or empty.
     */
    public static int[] notEmpty(int[] array) {
        return validateArray(array);
    }

    /**
     * Enforce a not-null and not-empty precondition on an array.
     * 
     * @param array
     *            Can't be null or empty
     * @return The argument itself
     * @throws IllegalArgumentException
     *             if array is null or empty.
     */
    public static long[] notEmpty(long[] array) {
        return validateArray(array);
    }

    /**
     * Enforce a not-null and not-empty precondition on an array.
     * 
     * @param array
     *            Can't be null or empty
     * @return The argument itself
     * @throws IllegalArgumentException
     *             if array is null or empty.
     */
    public static float[] notEmpty(float[] array) {
        return validateArray(array);
    }

    /**
     * Enforce a not-null and not-empty precondition on an array.
     * 
     * @param array
     *            Can't be null or empty
     * @return The argument itself
     * @throws IllegalArgumentException
     *             if array is null or empty.
     */
    public static double[] notEmpty(double[] array) {
        return validateArray(array);
    }

    /**
     * Enforce a not-null and not-empty precondition on an array.
     * 
     * @param array
     *            Can't be null or empty
     * @return The argument itself
     * @throws IllegalArgumentException
     *             if array is null or empty.
     */
    public static boolean[] notEmpty(boolean[] array) {
        return validateArray(array);
    }

    /**
     * Enforce a not-null and not-empty precondition on an array.
     * 
     * @param array
     *            Can't be null or empty
     * @return The argument itself
     * @throws IllegalArgumentException
     *             if array is null or empty.
     */
    public static <T> T[] notEmpty(T[] array) {
        return validateArray(array);
    }

    /**
     * Enforce a not-null/not-empty precondition on a {@link String}.
     * 
     * @param s
     *            Can't be null or empty
     * @return The argument itself
     * @throws IllegalArgumentException
     *             If the precondition in s do not hold.
     */
    public static <T extends CharSequence> T notEmpty(T s) {
        notNull(s);
        if (s.length() <= 0) {
            throw new IllegalArgumentException("empty");
        }
        return s;
    }

    /**
     * Enforce a not-null/not-empty/not-blank precondition on a {@link String}.
     * 
     * @param s
     *            Can't be null, empty or only blanks
     * @return The argument itself
     * @throws IllegalArgumentException
     *             If the precondition in s do not hold.
     */
    public static String notBlank(String s) {
        notEmpty(s);
        if (s.trim().length() <= 0) {
            throw new IllegalArgumentException("blank");
        }
        return s;
    }

    /**
     * Enforce that the value is zero or greater.
     * 
     * @param value
     *            Can't be negative.
     * @return The value
     * @throws IllegalArgumentException
     *             If the precondition in value do not hold.
     */
    public static int nonNegative(int value) {
        if (value < 0) {
            throw new IllegalArgumentException("negative");
        }
        return value;
    }

    /**
     * Enforce that the value is greater than zero.
     * 
     * @param value
     *            Can't be zero or negative.
     * @return The value
     * @throws IllegalArgumentException
     *             If the precondition in value do not hold.
     */
    public static int positive(int value) {
        if (value <= 0) {
            throw new IllegalArgumentException("non-positive");
        }
        return value;
    }

    /**
     * Enforce that the value is zero or smaller.
     * 
     * @param value
     *            Can't be positive.
     * @return The value
     * @throws IllegalArgumentException
     *             If the precondition in value do not hold.
     */
    public static int nonPositive(int value) {
        if (value > 0) {
            throw new IllegalArgumentException("negative");
        }
        return value;
    }

    /**
     * Enforce that the value is smaller than zero.
     * 
     * @param value
     *            Can't be zero or positive.
     * @return The value
     * @throws IllegalArgumentException
     *             If the precondition in value do not hold.
     */
    public static int negative(int value) {
        if (value >= 0) {
            throw new IllegalArgumentException("non-negative");
        }
        return value;
    }

    /**
     * Enforce that the value is < a max.
     * 
     * @param value
     *            The value
     * @param max
     *            The max for the value.
     * @return The value
     * @throws IllegalArgumentException
     *             If the precondition in value do not hold.
     */
    public static int lt(int value, int max) {
        if (value >= max) {
            throw new IllegalArgumentException(value + " >= " + max);
        }
        return value;
    }

    /**
     * Enforce that the value is > a min.
     * 
     * @param value
     *            The value
     * @param min
     *            The max for the value.
     * @return The value
     * @throws IllegalArgumentException
     *             If the precondition in value do not hold.
     */
    public static int gt(int value, int min) {
        if (value <= min) {
            throw new IllegalArgumentException(value + " <= " + min);
        }
        return value;
    }

    /**
     * Enforce that the value is <= a max.
     * 
     * @param value
     *            The value
     * @param max
     *            The max for the value.
     * @return The value
     * @throws IllegalArgumentException
     *             If the precondition in value do not hold.
     */
    public static int le(int value, int max) {
        if (value > max) {
            throw new IllegalArgumentException(value + " > " + max);
        }
        return value;
    }

    /**
     * Enforce that the value is >= a min.
     * 
     * @param value
     *            The value
     * @param min
     *            The max for the value.
     * @return The value
     * @throws IllegalArgumentException
     *             If the precondition in value do not hold.
     */
    public static int ge(int value, int min) {
        if (value < min) {
            throw new IllegalArgumentException(value + " < " + min);
        }
        return value;
    }

    /**
     * Enforce that the value is even.
     * 
     * @param value
     *            The value.
     * @return The value itself.
     * @throws IllegalArgumentException
     *             if the value is not an even number.
     */
    public static int even(int value) {
        if (value % 2 != 0) {
            throw new IllegalArgumentException(value + " % 2 != 0");
        }
        return value;
    }

    /**
     * Enforce that the value is odd.
     * 
     * @param value
     *            The value.
     * @return The value itself.
     * @throws IllegalArgumentException
     *             if the value is not an odd number.
     */
    public static int odd(int value) {
        if (value % 2 == 0) {
            throw new IllegalArgumentException(value + " % 2 == 0");
        }
        return value;
    }
}
