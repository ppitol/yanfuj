package yanfuj.base;

public final class Some<T> extends Option<T> {

	public static <T> Some<T> some(T value) {
		return new Some<T>(value);
	}
	
	private final T value;
	
	public Some(T value) {
		this.value = value;
	}
	
	@Override
	public boolean hasValue() {
		return true;
	}

	@Override
	public T get() {
		return value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Some<?> other = (Some<?>) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Some[" + value + "]";
	}
}
