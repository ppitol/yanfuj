package yanfuj.base;

import java.util.Comparator;

/**
 * Provide some useful comparators.
 *
 * @author pitol
 *
 */
public final class Comparators {

	public static final Comparator<Integer> intAscComp = new Comparator<Integer>() {
		public int compare(Integer o1, Integer o2) {
			return o1.compareTo(o2);
		}
	};

	public static final Comparator<Integer> intDescComp = new Comparator<Integer>() {
		public int compare(Integer o1, Integer o2) {
			return o2.compareTo(o1);
		}
	};

	public static final Comparator<Long> longAscComp = new Comparator<Long>() {
		public int compare(Long o1, Long o2) {
			return o1.compareTo(o2);
		}
	};

	public static final Comparator<Long> longDescComp = new Comparator<Long>() {
		public int compare(Long o1, Long o2) {
			return o2.compareTo(o1);
		}
	};

	public static final Comparator<String> strAscComp = new Comparator<String>() {
		public int compare(String o1, String o2) {
			return o1.compareTo(o2);
		}
	};

	public static final Comparator<String> strDescComp = new Comparator<String>() {
		public int compare(String o1, String o2) {
			return o2.compareTo(o1);
		}
	};

	public static <T extends Comparable<T>> Comparator<T> comparable() {
		return  new Comparator<T>() {
			public int compare(T o1, T o2) {
				return o1 == null ? (o2 == null ? 0 : 1) : o1.compareTo(o2);
			}
		};
	}

	private Comparators() {}
}
