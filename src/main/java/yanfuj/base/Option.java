package yanfuj.base;

public abstract class Option<T> {

	public static <T> Option<T> option(T value) {
		if (value == null) {
			return None.none();
		}
		return Some.some(value);
	}
	
	public abstract boolean hasValue();
	
	public abstract T get();
	
	public T getOrElse(T alternative) {
		return hasValue() ? get() : alternative;
	}
}
