package yanfuj.base;

public final class None<T> extends Option<T> {

	public static class NoneHasNoValueException extends RuntimeException {
		private static final long serialVersionUID = 1L;
	}
	
	@SuppressWarnings("rawtypes")
	private static final None<?> NONE = new None();
	
	@SuppressWarnings("unchecked")
	public static <T> None<T> none() {
		return (None<T>) NONE;
	}
	
	private None() {
		super();
	}
	
	@Override
	public boolean hasValue() {
		return false;
	}

	@Override
	public T get() {
		throw new NoneHasNoValueException();
	}

	@Override
	public int hashCode() {
		return -1;
	}

	@Override
	public boolean equals(Object obj) {
		return obj == NONE;
	}

	@Override
	public String toString() {
		return "None";
	}
}
