package yanfuj.functions;

/**
 * Evaluate a boolean condition over some object.
 *
 * @author pitol
 *
 * @param <T>
 *            type of the object over which the condition is evaluated.
 */
public interface Predicate<T> extends F<T, Boolean> {
}
