package yanfuj.functions;

/**
 * A procedure that takes four arguments and can throw an exception.
 *
 * @author pitol
 *
 * @param <A1>
 *            The first argument's type.
 * @param <A2>
 *            The second argument's type.
 * @param <A3>
 *            The third argument's type.
 * @param <A4>
 *            The fourth argument's type.
 * @param <E>
 *            The Exception's type
 */
public interface Pe4<A1, A2, A3, A4, E extends Exception> {
	public void exec(A1 arg1, A2 arg2, A3 arg3, A4 arg4) throws E;
}
