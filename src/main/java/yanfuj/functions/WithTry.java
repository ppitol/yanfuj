package yanfuj.functions;

import java.lang.reflect.Constructor;

public final class WithTry<E extends RuntimeException> {

	public static <E extends RuntimeException> WithTry<E> targetingException(Class<E> exceptionClass) {
		try {
			return new WithTry<E>(exceptionClass);
		} catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private final Constructor<E> exceptionConstructor;

	private WithTry(Class<E> exceptionClass) {
		try {
			this.exceptionConstructor = exceptionClass.getConstructor(Throwable.class);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private <R> R throwRuntimeException(Exception e) {
		try {
			throw exceptionConstructor.newInstance(e);
		} catch (Exception e1) {
			throw new RuntimeException("Constructor failure when handling " + e.getMessage(), e1);
		}
	}

	public final <R> R withTry(Supplier<R> supplier) throws E {
		try {
			return supplier.get();
		} catch (Exception e) {
			return throwRuntimeException(e);
		}
	}

	public final <R, Ex extends Exception> R withTry(SupplierE<R, Ex> supplier) throws E {
		try {
			return supplier.get();
		} catch (Exception e) {
			return throwRuntimeException(e);
		}
	}

	public final <A, T> T withTry(F<A, T> function, A arg) throws E {
		try {
			return function.apply(arg);
		} catch (Exception e) {
			return throwRuntimeException(e);
		}
	}

	public final <A, T, Ex extends Exception> T withTry(Fe<A, T, Ex> function, A arg) throws E {
		try {
			return function.apply(arg);
		} catch (Exception e) {
			return throwRuntimeException(e);
		}
	}

	public final <A1, A2, T> T withTry(F2<A1, A2, T> function, A1 arg1, A2 arg2) throws E {
		try {
			return function.apply(arg1, arg2);
		} catch (Exception e) {
			return throwRuntimeException(e);
		}
	}

	public final <A1, A2, T, Ex extends Exception> T withTry(Fe2<A1, A2, T, Ex> function, A1 arg1, A2 arg2) throws E {
		try {
			return function.apply(arg1, arg2);
		} catch (Exception e) {
			return throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, T> T withTry(F3<A1, A2, A3, T> function, A1 arg1, A2 arg2, A3 arg3) throws E {
		try {
			return function.apply(arg1, arg2, arg3);
		} catch (Exception e) {
			return throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, T, Ex extends Exception> T withTry(Fe3<A1, A2, A3, T, Ex> function, A1 arg1, A2 arg2, A3 arg3) throws E {
		try {
			return function.apply(arg1, arg2, arg3);
		} catch (Exception e) {
			return throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4, T> T withTry(F4<A1, A2, A3, A4, T> function, A1 arg1, A2 arg2, A3 arg3, A4 arg4) throws E {
		try {
			return function.apply(arg1, arg2, arg3, arg4);
		} catch (Exception e) {
			return throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4, T, Ex extends Exception> T withTry(Fe4<A1, A2, A3, A4, T, Ex> function, A1 arg1, A2 arg2, A3 arg3, A4 arg4) throws E {
		try {
			return function.apply(arg1, arg2, arg3, arg4);
		} catch (Exception e) {
			return throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4, A5, T> T withTry(F5<A1, A2, A3, A4, A5, T> function, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5) throws E {
		try {
			return function.apply(arg1, arg2, arg3, arg4, arg5);
		} catch (Exception e) {
			return throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4, A5, T, Ex extends Exception> T withTry(Fe5<A1, A2, A3, A4, A5, T, Ex> function, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5) throws E {
		try {
			return function.apply(arg1, arg2, arg3, arg4, arg5);
		} catch (Exception e) {
			return throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4, A5, A6, T> T withTry(F6<A1, A2, A3, A4, A5, A6, T> function, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6) throws E {
		try {
			return function.apply(arg1, arg2, arg3, arg4, arg5, arg6);
		} catch (Exception e) {
			return throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4, A5, A6, T, Ex extends Exception> T withTry(Fe6<A1, A2, A3, A4, A5, A6, T, Ex> function, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6) throws E {
		try {
			return function.apply(arg1, arg2, arg3, arg4, arg5, arg6);
		} catch (Exception e) {
			return throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4, A5, A6, A7, T> T withTry(F7<A1, A2, A3, A4, A5, A6, A7, T> function, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6, A7 arg7) throws E {
		try {
			return function.apply(arg1, arg2, arg3, arg4, arg5, arg6, arg7);
		} catch (Exception e) {
			return throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4, A5, A6, A7, T, Ex extends Exception> T withTry(Fe7<A1, A2, A3, A4, A5, A6, A7, T, Ex> function, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6, A7 arg7) throws E {
		try {
			return function.apply(arg1, arg2, arg3, arg4, arg5, arg6, arg7);
		} catch (Exception e) {
			return throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4, A5, A6, A7, A8, T> T withTry(F8<A1, A2, A3, A4, A5, A6, A7, A8, T> function, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6, A7 arg7, A8 arg8) throws E {
		try {
			return function.apply(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
		} catch (Exception e) {
			return throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4, A5, A6, A7, A8, T, Ex extends Exception> T withTry(Fe8<A1, A2, A3, A4, A5, A6, A7, A8, T, Ex> function, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6, A7 arg7, A8 arg8) throws E {
		try {
			return function.apply(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
		} catch (Exception e) {
			return throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4, A5, A6, A7, A8, A9, T> T withTry(F9<A1, A2, A3, A4, A5, A6, A7, A8, A9, T> function, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6, A7 arg7, A8 arg8, A9 arg9) throws E {
		try {
			return function.apply(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);
		} catch (Exception e) {
			return throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4, A5, A6, A7, A8, A9, T, Ex extends Exception> T withTry(Fe9<A1, A2, A3, A4, A5, A6, A7, A8, A9, T, Ex> function, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6, A7 arg7, A8 arg8, A9 arg9) throws E {
		try {
			return function.apply(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);
		} catch (Exception e) {
			return throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, T> T withTry(F10<A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, T> function, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6, A7 arg7, A8 arg8, A9 arg9, A10 arg10) throws E {
		try {
			return function.apply(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10);
		} catch (Exception e) {
			return throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, T, Ex extends Exception> T withTry(Fe10<A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, T, Ex> function, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6, A7 arg7, A8 arg8, A9 arg9, A10 arg10) throws E {
		try {
			return function.apply(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10);
		} catch (Exception e) {
			return throwRuntimeException(e);
		}
	}

	public final void withTry(P procedure) throws E {
		try {
			procedure.exec();
		} catch (Exception e) {
			throwRuntimeException(e);
		}
	}

	public final <Ex extends Exception> void withTry(Pe<Ex> procedure) throws E {
		try {
			procedure.exec();
		} catch (Exception e) {
			throwRuntimeException(e);
		}
	}

	public final <A> void withTry(P1<A> procedure, A arg) throws E {
		try {
			procedure.exec(arg);
		} catch (Exception e) {
			throwRuntimeException(e);
		}
	}

	public final <A, Ex extends Exception> void withTry(Pe1<A, Ex> procedure, A arg) throws E {
		try {
			procedure.exec(arg);
		} catch (Exception e) {
			throwRuntimeException(e);
		}
	}

	public final <A1, A2> void withTry(P2<A1, A2> procedure, A1 arg1, A2 arg2) throws E {
		try {
			procedure.exec(arg1, arg2);
		} catch (Exception e) {
			throwRuntimeException(e);
		}
	}

	public final <A1, A2, Ex extends Exception> void withTry(Pe2<A1, A2, Ex> procedure, A1 arg1, A2 arg2) throws E {
		try {
			procedure.exec(arg1, arg2);
		} catch (Exception e) {
			throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3> void withTry(P3<A1, A2, A3> procedure, A1 arg1, A2 arg2, A3 arg3) throws E {
		try {
			procedure.exec(arg1, arg2, arg3);
		} catch (Exception e) {
			throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, Ex extends Exception> void withTry(Pe3<A1, A2, A3, Ex> procedure, A1 arg1, A2 arg2, A3 arg3) throws E {
		try {
			procedure.exec(arg1, arg2, arg3);
		} catch (Exception e) {
			throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4> void withTry(P4<A1, A2, A3, A4> procedure, A1 arg1, A2 arg2, A3 arg3, A4 arg4) throws E {
		try {
			procedure.exec(arg1, arg2, arg3, arg4);
		} catch (Exception e) {
			throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4, Ex extends Exception> void withTry(Pe4<A1, A2, A3, A4, Ex> procedure, A1 arg1, A2 arg2, A3 arg3, A4 arg4) throws E {
		try {
			procedure.exec(arg1, arg2, arg3, arg4);
		} catch (Exception e) {
			throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4, A5> void withTry(P5<A1, A2, A3, A4, A5> procedure, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5) throws E {
		try {
			procedure.exec(arg1, arg2, arg3, arg4, arg5);
		} catch (Exception e) {
			throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4, A5, Ex extends Exception> void withTry(Pe5<A1, A2, A3, A4, A5, Ex> procedure, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5) throws E {
		try {
			procedure.exec(arg1, arg2, arg3, arg4, arg5);
		} catch (Exception e) {
			throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4, A5, A6> void withTry(P6<A1, A2, A3, A4, A5, A6> procedure, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6) throws E {
		try {
			procedure.exec(arg1, arg2, arg3, arg4, arg5, arg6);
		} catch (Exception e) {
			throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4, A5, A6, Ex extends Exception> void withTry(Pe6<A1, A2, A3, A4, A5, A6, Ex> procedure, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6) throws E {
		try {
			procedure.exec(arg1, arg2, arg3, arg4, arg5, arg6);
		} catch (Exception e) {
			throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4, A5, A6, A7> void withTry(P7<A1, A2, A3, A4, A5, A6, A7> procedure, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6, A7 arg7) throws E {
		try {
			procedure.exec(arg1, arg2, arg3, arg4, arg5, arg6, arg7);
		} catch (Exception e) {
			throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4, A5, A6, A7, Ex extends Exception> void withTry(Pe7<A1, A2, A3, A4, A5, A6, A7, Ex> procedure, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6, A7 arg7) throws E {
		try {
			procedure.exec(arg1, arg2, arg3, arg4, arg5, arg6, arg7);
		} catch (Exception e) {
			throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4, A5, A6, A7, A8> void withTry(P8<A1, A2, A3, A4, A5, A6, A7, A8> procedure, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6, A7 arg7, A8 arg8) throws E {
		try {
			procedure.exec(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
		} catch (Exception e) {
			throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4, A5, A6, A7, A8, Ex extends Exception> void withTry(Pe8<A1, A2, A3, A4, A5, A6, A7, A8, Ex> procedure, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6, A7 arg7, A8 arg8) throws E {
		try {
			procedure.exec(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
		} catch (Exception e) {
			throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4, A5, A6, A7, A8, A9> void withTry(P9<A1, A2, A3, A4, A5, A6, A7, A8, A9> procedure, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6, A7 arg7, A8 arg8, A9 arg9) throws E {
		try {
			procedure.exec(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);
		} catch (Exception e) {
			throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4, A5, A6, A7, A8, A9, Ex extends Exception> void withTry(Pe9<A1, A2, A3, A4, A5, A6, A7, A8, A9, Ex> procedure, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6, A7 arg7, A8 arg8, A9 arg9) throws E {
		try {
			procedure.exec(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);
		} catch (Exception e) {
			throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4, A5, A6, A7, A8, A9, A10> void withTry(P10<A1, A2, A3, A4, A5, A6, A7, A8, A9, A10> procedure, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6, A7 arg7, A8 arg8, A9 arg9, A10 arg10) throws E {
		try {
			procedure.exec(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10);
		} catch (Exception e) {
			throwRuntimeException(e);
		}
	}

	public final <A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, Ex extends Exception> void withTry(Pe10<A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, Ex> procedure, A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6, A7 arg7, A8 arg8, A9 arg9, A10 arg10) throws E {
		try {
			procedure.exec(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10);
		} catch (Exception e) {
			throwRuntimeException(e);
		}
	}

	public final <T, Ex extends Exception> Supplier<T> f(final SupplierE<T, Ex> fe) {
		return new Supplier<T>() {
			@Override
			public T get() {
				return withTry(fe);
			}
		};
	}

	public final <A, T, Ex extends Exception> F<A, T> f(final Fe<A, T, Ex> fe) {
		return new F<A, T>() {
			@Override
			public T apply(A arg) {
				return withTry(fe, arg);
			}
		};
	}

	public final <A1, A2, T, Ex extends Exception> F2<A1, A2, T> f(final Fe2<A1, A2, T, Ex> fe) {
		return new F2<A1, A2, T>() {
			@Override
			public T apply(A1 arg1, A2 arg2) {
				return withTry(fe, arg1, arg2);
			}
		};
	}

	public final <A1, A2, A3, T, Ex extends Exception> F3<A1, A2, A3, T> f(final Fe3<A1, A2, A3, T, Ex> fe) {
		return new F3<A1, A2, A3, T>() {
			@Override
			public T apply(A1 arg1, A2 arg2, A3 arg3) {
				return withTry(fe, arg1, arg2, arg3);
			}
		};
	}

	public final <A1, A2, A3, A4, T, Ex extends Exception> F4<A1, A2, A3, A4, T> f(final Fe4<A1, A2, A3, A4, T, Ex> fe) {
		return new F4<A1, A2, A3, A4, T>() {
			@Override
			public T apply(A1 arg1, A2 arg2, A3 arg3, A4 arg4) {
				return withTry(fe, arg1, arg2, arg3, arg4);
			}
		};
	}

	public final <A1, A2, A3, A4, A5, T, Ex extends Exception> F5<A1, A2, A3, A4, A5, T> f(final Fe5<A1, A2, A3, A4, A5, T, Ex> fe) {
		return new F5<A1, A2, A3, A4, A5, T>() {
			@Override
			public T apply(A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5) {
				return withTry(fe, arg1, arg2, arg3, arg4, arg5);
			}
		};
	}

	public final <A1, A2, A3, A4, A5, A6, T, Ex extends Exception> F6<A1, A2, A3, A4, A5, A6, T> f(final Fe6<A1, A2, A3, A4, A5, A6, T, Ex> fe) {
		return new F6<A1, A2, A3, A4, A5, A6, T>() {
			@Override
			public T apply(A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6) {
				return withTry(fe, arg1, arg2, arg3, arg4, arg5, arg6);
			}
		};
	}

	public final <A1, A2, A3, A4, A5, A6, A7, T, Ex extends Exception> F7<A1, A2, A3, A4, A5, A6, A7, T> f(final Fe7<A1, A2, A3, A4, A5, A6, A7, T, Ex> fe) {
		return new F7<A1, A2, A3, A4, A5, A6, A7, T>() {
			@Override
			public T apply(A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6, A7 arg7) {
				return withTry(fe, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
			}
		};
	}

	public final <A1, A2, A3, A4, A5, A6, A7, A8, T, Ex extends Exception> F8<A1, A2, A3, A4, A5, A6, A7, A8, T> f(final Fe8<A1, A2, A3, A4, A5, A6, A7, A8, T, Ex> fe) {
		return new F8<A1, A2, A3, A4, A5, A6, A7, A8, T>() {
			@Override
			public T apply(A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6, A7 arg7, A8 arg8) {
				return withTry(fe, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
			}
		};
	}

	public final <A1, A2, A3, A4, A5, A6, A7, A8, A9, T, Ex extends Exception> F9<A1, A2, A3, A4, A5, A6, A7, A8, A9, T> f(final Fe9<A1, A2, A3, A4, A5, A6, A7, A8, A9, T, Ex> fe) {
		return new F9<A1, A2, A3, A4, A5, A6, A7, A8, A9, T>() {
			@Override
			public T apply(A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6, A7 arg7, A8 arg8, A9 arg9) {
				return withTry(fe, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);
			}
		};
	}

	public final <A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, T, Ex extends Exception> F10<A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, T> f(final Fe10<A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, T, Ex> fe) {
		return new F10<A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, T>() {
			@Override
			public T apply(A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6, A7 arg7, A8 arg8, A9 arg9, A10 arg10) {
				return withTry(fe, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10);
			}
		};
	}
}
