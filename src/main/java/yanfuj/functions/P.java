package yanfuj.functions;

/**
 * A procedure object
 * 
 * @author pitol
 */
public interface P {
	public void exec();
}
