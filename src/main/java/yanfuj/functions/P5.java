package yanfuj.functions;

/**
 * A  procedure that takes five arguments.
 *
 * @author pitol
 *
 * @param <A1>
 *            The first argument's type.
 * @param <A2>
 *            The second argument's type.
 * @param <A3>
 *            The third argument's type.
 * @param <A4>
 *            The fourth argument's type.
 * @param <A5>
 *            The fifth argument's type.
 */
public interface P5<A1, A2, A3, A4, A5> {
	public void exec(A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5);
}
