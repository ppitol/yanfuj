package yanfuj.functions;

/**
 * A procedure object and can throw an exception.
 *
 * @author pitol
 *
 * @param <E>
 *            The Exception's type
 */
public interface Pe<E extends Exception> {
	public void exec() throws E;
}
