package yanfuj.functions;

/**
 * A  procedure that takes three arguments.
 *
 * @author pitol
 *
 * @param <A1>
 *            The first argument's type.
 * @param <A2>
 *            The second argument's type.
 * @param <A3>
 *            The third argument's type.
 */
public interface P3<A1, A2, A3> {
	public void exec(A1 arg1, A2 arg2, A3 arg3);
}
