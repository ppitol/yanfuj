package yanfuj.functions;

import static yanfuj.base.Some.some;

import java.util.Comparator;
import java.util.List;

/**
 * Common factories and utilities for functions.
 * 
 * @author pitol
 */
public final class Functions {

    /**
     * Supply always the same value.
     */
    public static <T> Supplier<T> constant(final T value) {
        return new Supplier<T>() {
            @Override
            public T get() {
                return value;
            }
        };
    }

    /**
     * Supply the elements from the {@link List}, cyclically.
     */
    public static <T> Supplier<T> cycle(final List<T> seq) {
        return new Supplier<T>() {
            private int pos = 0;

            @Override
            public T get() {
                if (pos == seq.size()) {
                    pos = 0;
                }
                return seq.get(pos++);
            }
        };
    }

    /**
     * Transform a {@link Comparator} in a {@link F2}.
     */
    public static <T> F2<T, T, Integer> fromComparator(final Comparator<? super T> comparator) {
        return new F2<T, T, Integer>() {
            @Override
            public Integer apply(T o1, T o2) {
                return comparator.compare(o1, o2);
            }
        };
    }

    /**
     * Transform a {@link F2} in a {@link Comparator}
     */
    public static <T> Comparator<T> fromF2(final F2<T, T, Integer> func) {
        return new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                return func.apply(o1, o2);
            }
        };
    }

    private static final F<?, ?> identity = new F<Object, Object>() {
        @Override
        public Object apply(Object param) {
            return param;
        }
    };
    
    /**
     * @return the identity function.
     */
    @SuppressWarnings("unchecked")
    public static <T> F<T, T> identity() {
        return (F<T, T>) identity;
    }
    
    /**
     * Call {@link #toString()}, except for null when it returns the empty
     * {@link String}
     */
    public static final F<?, String> toString = new F<Object, String>() {
        @Override
        public String apply(Object param) {
            return some(param).getOrElse("").toString();
        }
    };

    /**
     * Partially apply a 1-arity function.
     */
    public static <A, T> Supplier<T> partial(final F<A, T> func, final A arg) {
        return new Supplier<T>() {
            @Override
            public T get() {
                return func.apply(arg);
            }
        };
    }

    /**
     * Partially apply a 2-arity function.
     */
    public static <A1, A, T> F<A, T> partial(final F2<A1, A, T> func, final A1 arg1) {
        return new F<A, T>() {
            @Override
            public T apply(A arg) {
                return func.apply(arg1, arg);
            }
        };
    }

    /**
     * Partially apply a 3-arity function on 2 arguments.
     */
    public static <A1, A2, A, T> F<A, T> partial(final F3<A1, A2, A, T> func, final A1 arg1, final A2 arg2) {
        return new F<A, T>() {
            @Override
            public T apply(A arg) {
                return func.apply(arg1, arg2, arg);
            }
        };
    }

    /**
     * Partially apply a 4-arity function on 3 arguments.
     */
    public static <A1, A2, A3, A, T> F<A, T> partial(final F4<A1, A2, A3, A, T> func, final A1 arg1,
            final A2 arg2, final A3 arg3) {
        return new F<A, T>() {
            @Override
            public T apply(A arg) {
                return func.apply(arg1, arg2, arg3, arg);
            }
        };
    }

    /**
     * Partially apply a 5-arity function on 4 arguments.
     */
    public static <A1, A2, A3, A4, A, T> F<A, T> partial(final F5<A1, A2, A3, A4, A, T> func, final A1 arg1,
            final A2 arg2, final A3 arg3, final A4 arg4) {
        return new F<A, T>() {
            @Override
            public T apply(A arg) {
                return func.apply(arg1, arg2, arg3, arg4, arg);
            }
        };
    }

    /**
     * Partially apply a 6-arity function on 5 arguments.
     */
    public static <A1, A2, A3, A4, A5, A, T> F<A, T> partial(final F6<A1, A2, A3, A4, A5, A, T> func,
            final A1 arg1, final A2 arg2, final A3 arg3, final A4 arg4, final A5 arg5) {
        return new F<A, T>() {
            @Override
            public T apply(A arg) {
                return func.apply(arg1, arg2, arg3, arg4, arg5, arg);
            }
        };
    }

    /**
     * Partially apply a 7-arity function on 6 arguments.
     */
    public static <A1, A2, A3, A4, A5, A6, A, T> F<A, T> partial(final F7<A1, A2, A3, A4, A5, A6, A, T> func,
            final A1 arg1, final A2 arg2, final A3 arg3, final A4 arg4, final A5 arg5, final A6 arg6) {
        return new F<A, T>() {
            @Override
            public T apply(A arg) {
                return func.apply(arg1, arg2, arg3, arg4, arg5, arg6, arg);
            }
        };
    }

    /**
     * Partially apply a 8-arity function on 7 arguments.
     */
    public static <A1, A2, A3, A4, A5, A6, A7, A, T> F<A, T> partial(
            final F8<A1, A2, A3, A4, A5, A6, A7, A, T> func, final A1 arg1, final A2 arg2, final A3 arg3,
            final A4 arg4, final A5 arg5, final A6 arg6, final A7 arg7) {
        return new F<A, T>() {
            @Override
            public T apply(A arg) {
                return func.apply(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg);
            }
        };
    }

    /**
     * Partially apply a 9-arity function on 8 arguments.
     */
    public static <A1, A2, A3, A4, A5, A6, A7, A8, A, T> F<A, T> partial(
            final F9<A1, A2, A3, A4, A5, A6, A7, A8, A, T> func, final A1 arg1, final A2 arg2, final A3 arg3,
            final A4 arg4, final A5 arg5, final A6 arg6, final A7 arg7, final A8 arg8) {
        return new F<A, T>() {
            @Override
            public T apply(A arg) {
                return func.apply(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg);
            }
        };
    }

    /**
     * Partially apply a 10-arity function on 9 arguments.
     */
    public static <A1, A2, A3, A4, A5, A6, A7, A8, A9, A, T> F<A, T> partial(
            final F10<A1, A2, A3, A4, A5, A6, A7, A8, A9, A, T> func, final A1 arg1, final A2 arg2,
            final A3 arg3, final A4 arg4, final A5 arg5, final A6 arg6, final A7 arg7, final A8 arg8,
            final A9 arg9) {
        return new F<A, T>() {
            @Override
            public T apply(A arg) {
                return func.apply(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg);
            }
        };
    }

    /**
     * Function composition: outter(inner(arg)).
     */
    public static <Arg, Inter, T> F<Arg, T> compose(final F<Inter, T> outter, final F<Arg, Inter> inner) {
        return new F<Arg, T>() {
            @Override
            public T apply(Arg param) {
                return outter.apply(inner.apply(param));
            }
        };
    }

    private Functions() {
        super();
    }
}
