package yanfuj.functions;

/**
 * A procedure that take 1 argument.
 *
 * @author pitol
 *
 * @param <A>
 *            The argument's type.
 */
public interface P1<A> {
	public void exec(A arg);
}
