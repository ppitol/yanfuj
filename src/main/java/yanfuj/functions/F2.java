package yanfuj.functions;

/**
 * A function object for a function that take two arguments.
 * 
 * @author pitol
 * 
 * @param <A1>
 *            The first argument's type.
 * @param <A2>
 *            The second argument's type.
 * @param <T>
 *            The return type of the function.
 */
public interface F2<A1, A2, T> {
	public T apply(A1 arg1, A2 arg2);
}
