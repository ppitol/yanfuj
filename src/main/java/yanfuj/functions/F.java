package yanfuj.functions;

/**
 * A function object for a function that take one argument.
 * 
 * @author pitol
 * 
 * @param <A>
 *            The argument type of the function.
 * @param <T>
 *            The return type of the function.
 */
public interface F<A, T> {
	public T apply(A param);
}
