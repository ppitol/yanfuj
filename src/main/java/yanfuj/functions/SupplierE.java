package yanfuj.functions;

/**
 * Supply objects in some way, and can throw an exception..
 * 
 * @author pitol
 * 
 * @param <T>
 *            The supplied object`s type
 * @param <E>
 *            The Exception's type
 */
public interface SupplierE<T, E extends Exception> {
	T get() throws E;
}
