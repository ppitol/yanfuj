package yanfuj.functions;

/**
 * A function object for a function that take two arguments, launching an
 * {@link Exception}.
 *
 * @author pitol
 *
 * @param <A1>
 *            The first argument's type.
 * @param <A2>
 *            The second argument's type.
 * @param <T>
 *            The return type of the function.
 * @param <E>
 *            The Exception's type
 */
public interface Fe2<A1, A2, T, E extends Exception> {
	public T apply(A1 arg1, A2 arg2) throws E;
}
