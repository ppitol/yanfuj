package yanfuj.functions;

/**
 * A function object for a function that take one argument, launching an {@link Exception}.
 *
 * @author pitol
 *
 * @param <A>
 *            The argument type of the function.
 * @param <T>
 *            The return type of the function.
 * @param <E>
 *            The Exception's type
 */
public interface Fe<A, T, E extends Exception> {
	public T apply(A param) throws E;
}
