package yanfuj.functions;

/**
 * Common factories and utilities for predicates.
 * 
 * @author pitol
 */
public final class Predicates {

    /**
     * Is it null?
     */
    public static Predicate<?> notNull = new Predicate<Object>() {
        @Override
        public Boolean apply(Object param) {
            return param != null;
        }
    };

    /**
     * Always false.
     */
    public static <T> Predicate<T> falsy() {
        return new Predicate<T>() {
            @Override
            public Boolean apply(T param) {
                return false;
            }
        };
    }

    /**
     * Always true.
     */
    public static <T> Predicate<T> trutty() {
        return new Predicate<T>() {
            @Override
            public Boolean apply(T param) {
                return true;
            }
        };
    }

    /**
     * Negate the predicate.
     */
    public static <T> Predicate<T> not(final F<? super T, Boolean> predicate) {
        return new Predicate<T>() {
            @Override
            public Boolean apply(T param) {
                return !predicate.apply(param);
            }
        };
    }

    /**
     * Left && Right
     */
    public static <T> Predicate<T> and(final F<? super T, Boolean> left, final F<? super T, Boolean> right) {
        return new Predicate<T>() {
            @Override
            public Boolean apply(T param) {
                return left.apply(param) && right.apply(param);
            }
        };
    }

    /**
     * Left || Right
     */
    public static <T> Predicate<T> or(final F<? super T, Boolean> left, final F<? super T, Boolean> right) {
        return new Predicate<T>() {
            @Override
            public Boolean apply(T param) {
                return left.apply(param) || right.apply(param);
            }
        };
    }

    /**
     * Left ^ Right
     */
    public static <T> Predicate<T> xor(final F<? super T, Boolean> left, final F<? super T, Boolean> right) {
        return new Predicate<T>() {
            @Override
            public Boolean apply(T param) {
                return left.apply(param) ^ right.apply(param);
            }
        };
    }

    /**
     * Is it identical to?
     */
    public static <T> Predicate<T> same(final T value) {
        return new Predicate<T>() {
            @Override
            public Boolean apply(T param) {
                return param == value;
            }
        };
    }

    /**
     * Is it {@link #equals(Object)} to?
     */
    public static <T> Predicate<T> equals(final T value) {
        return new Predicate<T>() {
            @Override
            public Boolean apply(T param) {
                return param != null ? param.equals(value) : value == null;
            }
        };
    }

    /**
     * Is it even?
     */
    public static Predicate<Number> even = new Predicate<Number>() {
        @Override
        public Boolean apply(Number param) {
            return param.longValue() % 2 == 0;
        }
    };

    /**
     * Is it odd?
     */
    public static Predicate<Number> odd = new Predicate<Number>() {
        @Override
        public Boolean apply(Number param) {
            return param.longValue() % 2 != 0;
        }
    };

    /**
     * Is it greater than the value?
     */
    public static Predicate<Number> gt(final Number value) {
        return new Predicate<Number>() {
            @Override
            public Boolean apply(Number param) {
                return param.doubleValue() > value.doubleValue();
            }
        };
    }

    /**
     * Is it greater than or equal to the value?
     */
    public static Predicate<Number> ge(final Number value) {
        return new Predicate<Number>() {
            @Override
            public Boolean apply(Number param) {
                return param.doubleValue() >= value.doubleValue();
            }
        };
    }

    /**
     * Is it lesser than the value?
     */
    public static Predicate<Number> lt(final Number value) {
        return new Predicate<Number>() {
            @Override
            public Boolean apply(Number param) {
                return param.doubleValue() < value.doubleValue();
            }
        };
    }

    /**
     * Is it lesser than or equal to the value?
     */
    public static Predicate<Number> le(final Number value) {
        return new Predicate<Number>() {
            @Override
            public Boolean apply(Number param) {
                return param.doubleValue() <= value.doubleValue();
            }
        };
    }

    /**
     * Is it equal to the value?
     */
    public static Predicate<Number> eq(final Number value) {
        return same(value);
    }

    private Predicates() {}
}
