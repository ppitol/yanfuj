package yanfuj.functions;

/**
 * A function object for a function that take three arguments.
 *
 * @author pitol
 *
 * @param <A1>
 *            The first argument's type.
 * @param <A2>
 *            The second argument's type.
 * @param <A3>
 *            The third argument's type.
 * @param <T>
 *            The return type of the function.
 */
public interface F3<A1, A2, A3, T> {
	public T apply(A1 arg1, A2 arg2, A3 arg3);
}
