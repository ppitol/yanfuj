package yanfuj.functions;

/**
 * A function object for a function that take nine arguments, launching an
 * {@link Exception}.
 *
 * @author pitol
 *
 * @param <A1>
 *            The first argument's type.
 * @param <A2>
 *            The second argument's type.
 * @param <A3>
 *            The third argument's type.
 * @param <A4>
 *            The fourth argument's type.
 * @param <A5>
 *            The fifth argument's type.
 * @param <A6>
 *            The sixth argument's type.
 * @param <A7>
 *            The seventh argument's type.
 * @param <A8>
 *            The eighth argument's type.
 * @param <A9>
 *            The ninth argument's type.
 * @param <T>
 *            The return type of the function.
 * @param <E>
 *            The Exception's type
 */
public interface Fe9<A1, A2, A3, A4, A5, A6, A7, A8, A9, T, E extends Exception> {
	public T apply(A1 arg1, A2 arg2, A3 arg3, A4 arg4, A5 arg5, A6 arg6, A7 arg7, A8 arg8, A9 arg9) throws E;
}
