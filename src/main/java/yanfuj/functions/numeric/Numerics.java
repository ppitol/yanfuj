package yanfuj.functions.numeric;

import yanfuj.functions.F2;

public class Numerics {

    public static final F2<Integer, Integer, Integer> addInts = new F2<Integer, Integer, Integer>() {
        @Override
        public Integer apply(Integer arg1, Integer arg2) {
            return arg1.intValue() + arg2.intValue();
        }
    };

    public static final F2<Integer, Integer, Integer> subtractInts = new F2<Integer, Integer, Integer>() {
        @Override
        public Integer apply(Integer arg1, Integer arg2) {
            return arg1.intValue() - arg2.intValue();
        }
    };

    public static final F2<Integer, Integer, Integer> multiplyInts = new F2<Integer, Integer, Integer>() {
        @Override
        public Integer apply(Integer arg1, Integer arg2) {
            return arg1.intValue() * arg2.intValue();
        }
    };

    public static final F2<Integer, Integer, Integer> divideInts = new F2<Integer, Integer, Integer>() {
        @Override
        public Integer apply(Integer arg1, Integer arg2) {
            return arg1.intValue() / arg2.intValue();
        }
    };

    public static final F2<Integer, Integer, Integer> remainderInts= new F2<Integer, Integer, Integer>() {
        @Override
        public Integer apply(Integer arg1, Integer arg2) {
            return arg1.intValue() % arg2.intValue();
        }
    };

    public static final F2<Long, Long, Long> addLongs = new F2<Long, Long, Long>() {
        @Override
        public Long apply(Long arg1, Long arg2) {
            return arg1.longValue() + arg2.longValue();
        }
    };

    public static final F2<Long, Long, Long> subtractLongs = new F2<Long, Long, Long>() {
        @Override
        public Long apply(Long arg1, Long arg2) {
            return arg1.longValue() - arg2.longValue();
        }
    };

    public static final F2<Long, Long, Long> multiplyLongs = new F2<Long, Long, Long>() {
        @Override
        public Long apply(Long arg1, Long arg2) {
            return arg1.longValue() * arg2.longValue();
        }
    };

    public static final F2<Long, Long, Long> divideLongs = new F2<Long, Long, Long>() {
        @Override
        public Long apply(Long arg1, Long arg2) {
            return arg1.longValue() / arg2.longValue();
        }
    };

    public static final F2<Long, Long, Long> remainderLongs = new F2<Long, Long, Long>() {
        @Override
        public Long apply(Long arg1, Long arg2) {
            return arg1.longValue() % arg2.longValue();
        }
    };

    /**
     * Exponentiation
     */
    public static final F2<Double, Double, Double> pow = new F2<Double, Double, Double>() {
        @Override
        public Double apply(Double arg1, Double arg2) {
            return Math.pow(arg1.doubleValue(), arg2.doubleValue());
        }
    };
}
