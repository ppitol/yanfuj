package yanfuj.functions;

/**
 * Supply objects in some way.
 * 
 * @author pitol
 * 
 * @param <T>
 *            The supplied object`s type
 */
public interface Supplier<T> {
	T get();
}
