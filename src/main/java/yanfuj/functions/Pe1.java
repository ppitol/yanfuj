package yanfuj.functions;

/**
 * A procedure that take 1 argument and can throw an exception.
 *
 * @author pitol
 *
 * @param <A>
 *            The argument's type.
 * @param <E>
 *            The Exception's type
 */
public interface Pe1<A, E extends Exception> {
	public void exec(A arg) throws E;
}
