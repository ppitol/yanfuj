package yanfuj.functions;

/**
 * A procedure that takes two arguments.
 *
 * @author pitol
 *
 * @param <A1>
 *            The first argument's type.
 * @param <A2>
 *            The second argument's type.
 */
public interface P2<A1, A2> {
	public void exec(A1 arg1, A2 arg2);
}
