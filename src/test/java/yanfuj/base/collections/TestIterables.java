package yanfuj.base.collections;

import static java.util.Collections.emptyList;

import static org.fest.assertions.Assertions.assertThat;

import static yanfuj.base.None.none;
import static yanfuj.base.Some.some;
import static yanfuj.base.collections.Iterables.exclude;
import static yanfuj.base.collections.Iterables.filter;
import static yanfuj.base.collections.Iterables.first;
import static yanfuj.base.collections.Iterables.fold;
import static yanfuj.base.collections.Iterables.map;
import static yanfuj.base.collections.Iterables.reduce;
import static yanfuj.base.collections.Iterables.rest;
import static yanfuj.functions.Functions.identity;
import static yanfuj.functions.Predicates.even;
import static yanfuj.functions.numeric.Numerics.addInts;

import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.ImmutableList.of;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import com.google.common.collect.ImmutableSet;


public class TestIterables {

    @Test
    public void theFirstOfAColIsItsFirst() {
        Iterable<Integer> nums = of(1, 2, 3);
        assertThat(first(nums).get()).isEqualTo(1);
    }

    @Test
    public void theFirstOfAnEmptyColIsNone() {
        assertThat(first(emptyList())).isEqualTo(none());
    }


    @Test
    public void theFirstOfANullRefernceToAColIsNone() {
        assertThat(first(null)).isEqualTo(none());
    }

    @Test
    public void theRestOfAListIsItsRemainingElements() {
        List<Integer> actual = copyOf(rest(of(1, 2, 3)));
        assertThat(actual).isEqualTo(of(2, 3));
    }

    @Test
    public void theRestOfASetIsItsRemainingElements() {
        Set<Integer> set = ImmutableSet.of(3, 1, 2, 1, 3, 2);
        Set<Integer> actual = ImmutableSet.copyOf(rest(set));
        assertThat(actual.size()).isEqualTo(set.size() - 1);
        for (Integer i : actual) {
            assertThat(i).isIn(set);
        }
    }

    @Test
    public void theRestOfAnEmptyIterableIsEmpty() {
        assertThat(rest(emptyList())).isEmpty();
    }

    @Test
    public void theRestOfANullIterableReferenceIsEmpty() {
        assertThat(rest(null)).isEmpty();
    }

    @Test
    public void mapOfIdentityIsEqualToOriginal() {
        List<Integer> expected = of(1, 2, 3, 4);
        assertThat(copyOf(map(identity(), expected))).isEqualTo(expected);
    }

    @Test
    public void mapOfEmptyIsEmpty() {
        assertThat(copyOf(map(identity(), emptyList()))).isEqualTo(emptyList());
    }

    @Test
    public void mapOfNullIsEmpty() {
        assertThat(copyOf(map(identity(), null))).isEqualTo(emptyList());
    }

    @Test
    public void filterAnIterable() {
        assertThat(copyOf(
                filter(even, of(1, 2, 3, 4, 5, 6, 7, 8, 9))))
            .isEqualTo(
                of(2, 4, 6, 8));
    }

    @Test
    public void filterOfEmptyIsEmpty() {
        assertThat(copyOf(filter(even, Collections.<Number>emptyList()))).isEqualTo(emptyList());
    }

    @Test
    public void filterOfNummIsEmpty() {
        assertThat(copyOf(filter(even, null))).isEqualTo(emptyList());
    }

    @Test
    public void excludeAnIterable() {
        assertThat(copyOf(
                exclude(even, of(1, 2, 3, 4, 5, 6, 7, 8, 9))))
            .isEqualTo(
                of(1, 3, 5, 7, 9));
    }

    @Test
    public void excludeOfEmptyIsEmpty() {
        assertThat(copyOf(filter(even, Collections.<Number>emptyList()))).isEqualTo(emptyList());
    }

    @Test
    public void excludeOfNummIsEmpty() {
        assertThat(copyOf(filter(even, null))).isEqualTo(emptyList());
    }

    @Test
    public void reduceAnIterable() {
        assertThat(reduce(addInts, 0, of(1, 2, 3, 4))).isEqualTo(10);
    }

    @Test
    public void reduceOfEmptyIsTheSeed() {
        Integer one = Integer.valueOf(1);
        assertThat(reduce(addInts, one, Collections.<Integer>emptyList())).isSameAs(one);
    }

    @Test
    public void reduceOfNullIsTheSeed() {
        Integer one = Integer.valueOf(1);
        assertThat(reduce(addInts, one, null)).isSameAs(one);
    }

    @Test
    public void foldAnIterable() {
    	assertThat(fold(addInts, of(1, 2, 3, 4))).isEqualTo(some(10));
    }

    @Test
    public void foldOfEmptyIsNone() {
    	assertThat(fold(addInts, Collections.<Integer>emptyList())).isEqualTo(none());
    }

    @Test
    public void foldOfNullIsNone() {
    	assertThat(fold(addInts, null)).isEqualTo(none());
    }
}
